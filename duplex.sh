#!/bin/bash
# (C) 2005 David Villa
# "duplex" print in duplex mode in a standalone simplex printer 
	   
if [ -z "$1" ]; then
    echo "Usage: duplex <file_to_print> [pages_per_sheet]"
    exit
fi

filename=/tmp/file.ps
a2ps $2 $1 -o $filename

npages=$(grep -c "%%Page:" $filename)
let "resto = $npages % 2"
if [ "$resto" -eq 1 ]
then
    extra="-p1-,_" # odd number of pages, add a blank page at end
fi

psselect -o < $filename | lp
echo "Wait printer finish and the press ENTER to print even pages..."
read
psselect -e -r $extra < $filename | lp

rm $filename
